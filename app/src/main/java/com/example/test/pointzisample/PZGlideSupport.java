package com.example.test.pointzisample;
import android.app.Activity;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.streethawk.library.pointzi.Constants;
import com.streethawk.library.pointzi.GlideImageLoadListener;

public class PZGlideSupport implements Constants, GlideImageLoadListener {

    @Override
    public void onImageLoad(Activity activity, String resourceImg,
                            int placeHolderid, ImageView imageView,
                            int cornerRadiusImg, boolean isPreviewMode) {
        Boolean shouldSkipCache = false;
        DiskCacheStrategy cacheStrategy = DiskCacheStrategy.ALL;
        if (isPreviewMode) {
            shouldSkipCache = true;
            cacheStrategy = DiskCacheStrategy.NONE;
        }
        RequestOptions options = new RequestOptions();
        if (cornerRadiusImg == DEFAULT_CORNER_RADIUS) {
            options.diskCacheStrategy(cacheStrategy)
                .skipMemoryCache(shouldSkipCache)
                .placeholder(placeHolderid)
                .dontTransform()
                .priority(Priority.HIGH)
                .error(placeHolderid);

        } else {
            options.diskCacheStrategy(cacheStrategy)
                .transform(new PZGlideTransformation(activity, cornerRadiusImg))
                .skipMemoryCache(shouldSkipCache)
                .placeholder(placeHolderid)
                .priority(Priority.HIGH)
                .error(placeHolderid);
        }
        Glide.with(activity)
            .load(resourceImg)
            .apply(options)
            .into(imageView);
    }

    @Override
    public void onGlideCacheLoad(Activity activity, String imgUrl) {
        Glide.with(activity)
            .asDrawable()
            .load(imgUrl)
            .downloadOnly(
                Target.SIZE_ORIGINAL,
                Target.SIZE_ORIGINAL
            );
    }
}