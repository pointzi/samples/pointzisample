package com.example.test.pointzisample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import com.streethawk.library.core.SHCompositeOnClickListener;
import com.streethawk.library.core.StreetHawk;
import com.streethawk.library.pointzi.Pointzi;

public class MainActivity extends AppCompatActivity {

    private float xCoOrdinate, yCoOrdinate;
    ImageView pizzaImg;
    private final String SH_CUID = "sh_cuid";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pizzaImg = findViewById(R.id.pizzaManImg);

        // Initialise Pointzi, set your own AppKey
        Pointzi.INSTANCE.init(getApplication(), "wikipedia_demo");
        StreetHawk.INSTANCE.tagString(SH_CUID, "Pointzi User Adam");

        // Add onTouchListener Support for click and drags
        SHCompositeOnClickListener instance = SHCompositeOnClickListener.getInstance();
        instance.addOnClickListener(new PizzaManTouchListener(), pizzaImg);
        pizzaImg.setOnTouchListener(instance);
    }

    private final class PizzaManTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    xCoOrdinate = view.getX() - event.getRawX();
                    yCoOrdinate = view.getY() - event.getRawY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    view.animate().x(event.getRawX() + xCoOrdinate).y(event.getRawY() + yCoOrdinate).setDuration(0).start();
                    break;
                default:
                    return false;
            }
            return true;
        }
    }
}
